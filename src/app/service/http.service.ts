import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Swal from 'sweetalert2';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  base_url = environment.CONFIG.BASE_URL
  constructor(
    private http: HttpClient,
  ) { }


  getAllRecords() {
    return this.http.get(`${this.base_url}getAllRecords`)
  }

  addRecords(data: any) {
    return this.http.post(`${this.base_url}addNewRecord`, data)
  }

  deleteRecords(data: any) {
    return this.http.delete(`${this.base_url}deleteRecord/${data}`)
  }

  updateRecords(data: any) {
    return this.http.post(`${this.base_url}updateRecord`, data)
  }

  showDialog(message: any) {
    Swal.fire({
      // position: '',
      icon: 'success',
      title: message,
      showConfirmButton: false,
      timer: 1500
    })
  }
}

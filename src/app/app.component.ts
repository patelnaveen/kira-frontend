import { Component, Input, ViewChild } from '@angular/core';
import { TableComponent } from './table/table.component';
import { InputFormComponent } from './input-form/input-form.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild(TableComponent) tableComponent: any;
  @ViewChild(InputFormComponent) inputComponent: any;

  newRecordAdded(value: any) {
    this.tableComponent.getData();
  }

  editRecord(editRow: any) {
    this.inputComponent.editRecord(editRow)
  }

  deleteRecordData(deleteRow: any) {
    this.inputComponent.isEditDelete(deleteRow)
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from '../service/http.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Output() editRecordData = new EventEmitter<string>();
  @Output() deleteRecordData = new EventEmitter<string>();

  displayedColumns: string[] = ['position', 'name', 'email', 'phone', 'address', 'action'];
  dataSource = [];
  constructor(
    private httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.dataSource = [];
    this.httpService.getAllRecords().subscribe((res: any) => {
      this.dataSource = res.data
    },
      (err) => {
        console.log(err)
      })
  }

  editRecord(data: any) {
    this.editRecordData.emit(data);
  }

  deleteRecord(data: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete this record!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.httpService.deleteRecords(data._id).subscribe((res: any) => {
          this.httpService.showDialog(res.message)
          this.deleteRecordData.emit(data)
          this.getData();
        },
          (err) => {
            console.log(err)
          })
      }
    })
  }

}

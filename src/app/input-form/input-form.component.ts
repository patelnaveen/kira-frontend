import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from '../service/http.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss']
})
export class InputFormComponent implements OnInit {
  @Output() newRecordAdded = new EventEmitter<string>();
  userForm: any;
  editDataId: any = '';

  constructor(
    private httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.userFormControls();
  }

  userFormControls() {
    this.userForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
    });
  }

  addUser() {
    if (this.userForm.valid) {
      this.httpService.addRecords(this.userForm.value).subscribe((res: any) => {
        this.httpService.showDialog(res.message);
        this.newRecordAdded.emit(res);
        this.reset()
      },
        (err) => {
          console.log(err)
        })
    }
  }

  editRecord(data: any) {
    this.editDataId = data._id;
    this.userForm.setValue({
      name: data.name,
      email: data.email,
      phone: data.phone,
      address: data.address
    })
  }

  updateRecord() {
    if (this.userForm.valid) {
      let temp = this.userForm.value;
      temp["id"] = this.editDataId;
      this.httpService.updateRecords(temp).subscribe((res: any) => {
        this.httpService.showDialog(res.message);
        this.newRecordAdded.emit(res);
        this.reset()
      },
        (err) => {
          console.log(err)
        })
    }
  }

  isEditDelete(data: any) {
    if (this.editDataId) {
      this.reset()
    }
  }

  reset() {
    this.userForm.reset();
    this.editDataId = '';
  }
}
